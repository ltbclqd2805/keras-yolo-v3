from detectors import YOLO
import cv2
from PIL import Image
import copy
from tracker import Tracker
import numpy as np
from keras import backend as K
from timeit import default_timer as timer

ix, iy, ex, ey = -1, -1, -1, -1

def limit_mem():
    K.get_session().close()
    cfg = K.tf.ConfigProto()
    cfg.gpu_options.allow_growth = True
    K.set_session(K.tf.Session(config=cfg))

def draw_rec(event, x, y, flags, param):
    global ix, iy, ex, ey, drawing, mode

    if event == cv2.EVENT_LBUTTONDOWN:
        ix, iy = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        ex, ey = x, y
        cv2.rectangle(param, (ix, iy), (x, y), (0, 255, 0), 0)


def get_crop_size(video_path):
    cap = cv2.VideoCapture(video_path)
    while cap.isOpened():
        ret, frame = cap.read()
        cv2.namedWindow('draw_rectangle')
        cv2.setMouseCallback('draw_rectangle', draw_rec, frame)
        print("Choose your area of interest!")
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('a'):
                cv2.destroyAllWindows()
                # img_crop = frame[iy:ey, ix:ex]
                # cv2.imshow('crop', img_crop)
                # cv2.waitKey(0)
                break
        break



def main():
    limit_mem()
    path = 'camera.mp4'
    # Choose area of interest
    get_crop_size(path)
    print('Your area of interest: ', ix, ' ', iy, ' ', ex, ' ', ey)
    area = (ix, iy, ex, ey)

    # Create opencv video capture object
    cap = cv2.VideoCapture(path)
    w = int(cap.get(3))
    h = int(cap.get(4))
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    out = cv2.VideoWriter('res.avi', fourcc, 15, (w, h))

    # Create Object Detector
    detector = YOLO()

    # Create Object Tracker
    tracker = Tracker(iou_thresh=0.4, max_frames_to_skip=5, max_trace_length=20, trackIdCount=0)

    # Variables initialization
    track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                    (0, 255, 255), (255, 0, 255), (255, 127, 255),
                    (127, 0, 255), (127, 0, 127)]

    while cap.isOpened():
        # Capture frame-by-frame
        ret, frame = cap.read()

        frame = Image.fromarray(frame)

        # Detect and return centeroids of the objects in the frame
        result, centers, box_detected = detector.detect_image(frame, area)
        result = np.asarray(result)

        print('Number of detections: ', len(centers))

        # If centroids are detected then track them
        if len(box_detected) > 0:

            # Track object using Kalman Filter
            tracker.Update(box_detected)

            # For identified object tracks draw tracking line
            # Use various colors to indicate different track_id
            for i in range(len(tracker.tracks)):
                if len(tracker.tracks[i].trace) >= 5:
                    for j in range(len(tracker.tracks[i].trace)-1):
                        # Draw trace line
                        x1 = tracker.tracks[i].trace[j][0][0]
                        y1 = tracker.tracks[i].trace[j][1][0]
                        x2 = tracker.tracks[i].trace[j+1][0][0]
                        y2 = tracker.tracks[i].trace[j+1][1][0]
                        clr = tracker.tracks[i].track_id % 9
                        cv2.line(result, (int(x1), int(y1)), (int(x2), int(y2)),
                                 track_colors[clr], 2)

        # Display the resulting tracking frame
        cv2.rectangle(result, (ix, iy), (ex, ey), (0, 255, 0), 0)
        cv2.imshow('Tracking', result)
        out.write(result)


        # Check for key strokes
        k = cv2.waitKey(1) & 0xff
        if k == ord('n'):
            continue
        elif k == 27:  # 'esc' key has been pressed, exit program.
            break

    # When everything done, release the capture
    out.release()
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # execute main
    main()