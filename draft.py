import numpy as np
import cv2

kalman = cv2.KalmanFilter(7, 4, 0)

dt = 0.2

kalman.transitionMatrix = np.array(
    [[1, 0, 0, 0, dt, 0, 0], [0, 1, 0, 0, 0, dt, 0], [0, 0, 1, 0, 0, 0, dt],
     [0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 0, 1]],
    dtype=np.float32)
kalman.measurementMatrix = np.array(
    [[1, 0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0]],
    dtype=np.float32)

kalman.processNoiseCov = np.eye(7, dtype=np.float32)
kalman.processNoiseCov[-1, -1] *= 0.01
kalman.processNoiseCov[4:, 4:] *= 0.01

kalman.measurementNoiseCov = 0.1 * np.eye(4, dtype=np.float32)
kalman.measurementNoiseCov[2:, 2:] *= 10

kalman.errorCovPost = np.eye(7, dtype=np.float32)
kalman.errorCovPost[4:, 4:] *= 1000  # give high uncertainty to the unobservable initial velocities
kalman.errorCovPost *= 10
print(kalman.errorCovPost)
